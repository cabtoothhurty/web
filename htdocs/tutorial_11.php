<?php
define("NAME", "Hello, my name is Sarah!", true);
echo NAME;
echo name;

file_write("The quick brown fox jumped over the lazy dog!");
file_read();
echo "<h2>June 2018</h2>
    
<p>    CAB230 “Web Computing”
Workshop 11 – The PHP Language
Lecture Review Questions
Exercise 1 – Simple Calendar Web Application
page 2 / 7</p>";

display_month(6, 2018);

$array = array("28", "June", "2018");

display_birthdays(6, 2018, $array);

//year calendar
for ($month = 1; $month <= 12; $month++) {
    display_month($month, 2018);
    }

    function file_read() {
        $myfile = fopen("words.txt", "r") or die("Unable to open file!");
        echo fread($myfile, filesize("words.txt"));
        fclose($myfile);
        }

        function file_write($txt) {
            $myfile = fopen("words.txt", "w") or die("Unable to open file!");
            fwrite($myfile, $txt);
            fclose($myfile);
            //echo "The file was created!";
        }

    function display_month ($month, $year) {
        $first_day_of_month = mktime(0, 0, 0, $month, 1, $year);
        echo date('jS F Y', $first_day_of_month);
        $day_of_week_of_first_day_of_month = date('w', $first_day_of_month);
        $days_in_month = date('t', $first_day_of_month);
        $month_name = date('F', $first_day_of_month);

        echo "<table border=\"solid\">
        <tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>";


        $currentT = time() ;
        // 7 days; 24 hours; 60 mins; 60 secs
        $thisday = date('d', $currentT);

        if ($first_day_of_month > 0){
            echo "<tr><td colspan=\"".$day_of_week_of_first_day_of_month."\">&nbsp;</td>";
        } else {
            echo "<tr>";
        }

        for ($day_of_week = $day_of_week_of_first_day_of_month, $day_of_month=1;
            $day_of_month <= $days_in_month;
            $day_of_month++, $day_of_week++) {

            if ($day_of_week == 7) { # if past end of week
                echo '</tr>'; # end the row for the current week
                $day_of_week = 0; # reset to the start of the week
                echo '<tr>'; # create a row for the next week
            }

            //highlight today
            if ($day_of_month == $thisday){
                echo "<td><font color=\"Red\">".$day_of_month."</font></td>";
            } else {
                echo "<td>".$day_of_month."</td>";
            }

        }

        echo "
        </table>";
    }

    function display_birthdays ($month, $year, $array) {
        $first_day_of_month = mktime(0, 0, 0, $month, 1, $year);
        echo date('jS F Y', $first_day_of_month);
        $day_of_week_of_first_day_of_month = date('w', $first_day_of_month);
        $days_in_month = date('t', $first_day_of_month);
        $month_name = date('F', $first_day_of_month);

        echo "<table border=\"solid\">
        <tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>";


        $currentT = time() ;
        // 7 days; 24 hours; 60 mins; 60 secs
        $thisday = date('d', $currentT);

        if ($first_day_of_month > 0){
            echo "<tr><td colspan=\"".$day_of_week_of_first_day_of_month."\">&nbsp;</td>";
        } else {
            echo "<tr>";
        }

        for ($day_of_week = $day_of_week_of_first_day_of_month, $day_of_month=1;
            $day_of_month <= $days_in_month;
            $day_of_month++, $day_of_week++) {

            if ($day_of_week == 7) { # if past end of week
                echo '</tr>'; # end the row for the current week
                $day_of_week = 0; # reset to the start of the week
                echo '<tr>'; # create a row for the next week
            }

            $bday = $array[0];
            //hyperlink for birthday
            if ($year == $array[2]){
                if ($month_name == $array[1]){
                    if ($day_of_month == $bday){
                        echo "<td><a href=\"http://www.abc.net.au/news/archive/?date=".$year."-".$month."-".$bday."\"color=\"Red\">".$day_of_month."</a></td>";
                    } elseif ($day_of_month == $thisday){
                        echo "<td><font color=\"Red\">".$day_of_month."</font></td>";
                    } else {
                        echo "<td>".$day_of_month."</td>";
                    }
                }elseif ($day_of_month == $thisday){
                    echo "<td><font color=\"Red\">".$day_of_month."</font></td>";
                } else {
                    echo "<td>".$day_of_month."</td>";
                }
            } elseif ($day_of_month == $thisday){
                echo "<td><font color=\"Red\">".$day_of_month."</font></td>";
            } else {
                echo "<td>".$day_of_month."</td>";
            }



        }

        echo "
        </table>";
    }




?>