<!DOCTYPE html>
<html>
    
<?php
    //connect to database and get access to query functions.
    include 'include/database_connection.php';    
    include 'include/database_library.php';  
    include 'include/xml_map.php';

    session_start();
    //Header
	$page_title = "Wifi | Result";
    include 'include/header.php';
    
    if(isset($_POST['Review'])){
        include 'include/submitreview.php';
    }

?>
<body class="Individualbody">

        <?php include 'include/menu.php'; 
        
        if (isset($_GET['review'])){
            //run the user validation code
            include 'include/review_modal.php';
        }
        ?>
        <center>
                <div class="containerindivid" itemscope itemtype= "http://data-vocabulary.org/Property">
                
                <div class="layerindivid"></div>

                    <?php   
                        //use the get result to retrieve item from database
                        $name = $_GET['name'];

                        $result = getAllByNAme($name, $pdo);
                        createXML($result); 
                        $result = getAllByNAme($name, $pdo);

                        //display Title
                        $row = $result->fetch(); 
                        echo '<h2 class="h2-indiv" itemprop = "name">'.$_GET['name'].'</h2>';
                        
                        //display address
                        echo '<div class="headingindividual" itemprop ="address">'.$row[2].'</div>';
                        //echo '<div class="headingindividual">Davidadn St</div>';

                        //echo var_dump($row);
                        echo '<div class="starsindivid"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';

                        //print the number of stars as retrieved from the database 
                        $stars=(int)$row["Rating"];

                        //echo $stars;
                        for($i = 1; $i <= $stars; $i++){
                            echo '<span class="fa fa-star checked-item"></span>';
                        }                    
                        //print blank stars
                        for($i = (5-$stars); $i > 0; $i--){
                            echo '<span class="fa fa-star"></span>';
                        }

                        echo '</div>';
                    ?> 
                    <button type="button" class="button" ><a class="reviewbtn" href="http://localhost/individual.php?name=<?php echo $_GET['name']?>&review=true">Leave A review</a></button>
            </div>
        </center>

<!--         <div class="spacer">
        </div> -->

        <div class="spacer3">
        </div>

        <!--display description for particular location-->   
        
<!--         <div class="container-individ .col-xs-12">
 -->        <div class = "box .col-xs-12 .col-xsm-12">
                <h2>Description</h2>
                <?php
                    //Display the item Description
                    $row = getDescription($name, $pdo);
                    echo '<p>'.$row[0].'</p>';
                ?>
            </div>

            <div class="photobox ">
                <div class="photoheading">
                    <h2 >Photo Gallery</h2>
                </div>

                <div class = "gallery">
                    <img src="delaware3.jpeg" alt="Different Picture of location"class="gallery" href="delaware3.jpeg"  />
                    <img src="delaware4.jpg" alt="Different Picture of location 2" class="gallery" href="delaware4.jpg" />
                    <img src="delaware5.jpeg" alt="Another Different Picture of Location" class="gallery" href="delaware5.jpeg"	  />
                </div>
            </div>
            <!--User reviews    -->
			<div itemscope itemtype="http://data-vocabulary.org/Review">
            <div class="grid-container2 .col-xsm-12" itemprop = "itemReviewed">
                    <div class="grid-header"><b>User</b></div>
                    <div class="grid-header"><b>Review</b></div>
                    <div class="grid-header"><b>Rating</b></div>              
                <?php

                    $name = $_GET['name'];
                    $reviews = getReviews($name, $pdo);
                    //if no results are returned display the default review. 
                    if($reviews->rowCount() == 0){
                        echo '<div class="grid-results"></div>';
                        echo "<div class=\"grid-results\">Be the first to review this location!</div>";
                        echo '<div class="grid-results"></div>';

                    } 

                    //if rows returned display each one.
                    else {
                        //Display the legitmate reviews
                        $rows = $reviews->fetchAll(); 
                        
                        //for each review generate a grid item
                        foreach($rows as $row){
                            echo '<div class="grid-results"><img src="index.png" alt="profile photo" class="profilepic"></div>';
                            echo "<div class=\"grid-results\"itemprop = 'description' ><i>".$row[0]."</i></div>";
                            echo '<div class="grid-results" itemprop= "Rating"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';
                            //print the number of stars as retrieved from the database 
                            for($i = 1; $i <= $row[1]; $i++){
                                echo '<span class="fa fa-star checked"></span>';
                            }                    
                            //print blank stars

                            for($i = (5-$row[1]); $i > 0;$i--){
                                echo '<span class="fa fa-star"></span>';
                            }
                            echo '</div>';
                        }
                    }  
                ?>
                                
            </div>
			</div>
    <div class="spacer">
    </div>

    <div id="map" ></div>

    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQFVCVWMP-KN1vZ5BvtT4XvM3Ef4DDSS8&callback=initMap">
    </script>
    <script type="text/javascript" src="javascript/main.js"></script>
    
    <div class="spacer"></div>

<?php
	include 'include/footer.php';
    ?>	
</body>	

<footer>
</footer>
</html>