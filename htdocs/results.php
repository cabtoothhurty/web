<!DOCTYPE html>
<html>
<?php    
//Header
	$page_title = "Wifi | Results";
    include 'include/header.php';
    include 'include/xml_map.php';


?>
<body class="bodyresults">

            <?php 
                include 'include/menu.php';
            ?>
    <!-- <div class="bgimg-1">
        <div class="layer">
        </div> -->

        <div class="block no-padding">
            <div class="col-md-12">
                <div class="inner-header">
                <?php
                        echo '<h2>Wifi locations for '.implode("",$_POST).'</h2>';
                ?>
                        <a href="http://localhost/home.php" title="">Home</a></li> /
                        <a href="http://localhost/result.php" title="">Listing</a></li>
                </div>
            </div>
		</div>
        <div class="spacer">
                </div>
        <div class="results-div">
            <div class = "Column1">
                <div class="grid-container">
                    <div class="grid-header"><b>Image</b></div>
                    <div class="grid-header"><b>HotSpot Name</b></div>
                    <div class="grid-header"><b>Address</b></div>
                    <div class="grid-header"><b>Suburb</b></div>

                    <?php
                        //connect to database and get access to query functions.
                        include 'include/database_connection.php';    
                        include 'include/database_library.php'; 
                        include 'include/main.php';
   
                        $result = NULL;

                        //Check which Post variable is set from search and create sql query accordingly. 
                        if (isset($_POST['name']) ){
                            $name = htmlspecialchars($_POST['name']);
                            $result = getAllByNAme($name, $pdo);
                            createXML($result);
                            $result = getAllByNAme($name, $pdo);
 

                        }

                        if (isset($_POST['star']) ){
                            $star = htmlspecialchars($_POST['star']);
                            $result = getAllByStar($star, $pdo);
                            createXML($result);
                            $result = getAllByStar($star, $pdo);
 

                        }     

                        if (isset($_POST['Latitude']) ){
                            $lat = htmlspecialchars($_POST['Latitude']);
                            //echo var_dump($_POST);
                            if (isset($_POST['Longitude']) ){
                                $long =htmlspecialchars($_POST['Longitude']);
                                if ($lat!=0){
                                    if ($long !=0){
                                        $result = getAllByLocation($lat, $long, $pdo);  
                                        createXML($result); 
                                        $result = getAllByLocation($lat, $long, $pdo);  

       
                                    }
                                }
                            }
                        }
            
                        if (isset($_POST['suburb']) ){
                            $sub = htmlspecialchars($_POST['suburb']);
                            $result = getAllBySuburb($sub, $pdo);
                            createXML($result); 
                            $result = getAllBySuburb($sub, $pdo);


                        } 

                        //if a query was created then fetch the result and display in a table. 
                        if ($result != NULL){

                            $rows = $result->fetchAll(); 
                            if (count($rows) > 0){
                                foreach($rows as $row){
                                    echo '<div class="grid-results"><b><img class="result-img" src="delaware.jpg" alt="Delaware Street"></b></div>';
                                    //make wifi name hyperlink to indivdual page and submit name as get request
                                    echo '<div class="grid-results"><a class="links" href="http://localhost/individual.php?name='.$row[0].'">'.$row[0].'</a></div>';
                                    echo '<div class="grid-results"><a class="links" href="http://localhost/individual.php?name='.$row[0].'">'.$row[2].'</a></div>';
                                    echo '<div class="grid-results"><a class="links" href="http://localhost/individual.php?name='.$row[0].'">'.$row[1].'</a></div>';
                                }
                            } else { 
                                noResults($pdo);
                            }  
                        }  

                        // if no query was created (empty search form) display the default result's view. 
                        else { 
                            $result = NULL;
                            noResults($pdo);
                        }  
                    ?>

                </div>
        </div>
    </div>
    
<div class="spacer"></div>

    <div id="map" ></div>
                       
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQFVCVWMP-KN1vZ5BvtT4XvM3Ef4DDSS8&callback=initMap">
    </script>
    <script type="text/javascript" src="javascript/main.js"></script>

<div class="spacer"></div>

    <?php
        include 'include/footer.php';
    ?>
</body>
</html>