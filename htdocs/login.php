<!DOCTYPE html>
<html>

<?php 
//Code to register a user when redirected from register page with post

    //delete this in final project, if all pages are accessed from home page. 
    session_start();

    //Define error variables
    //$registered = false;
    $_SESSION['registered'] = false;
   // $username = $password = "";
    $_SESSION['username'] = $_SESSION['password'] = "";
    $_SESSION['username_err'] = $_SESSION['password_err'] = "";

   // $username_err = $password_err = "";
    //connect to database and get access to query functions.
    include 'include/database_connection.php';    
    include 'include/database_library.php'; 
    include 'include/main.php';
    
    // Processing form data when log in is submitted
    if($success = loginUser($_POST, $pdo)){
        $_SESSION['registered'] = true;
    }

    //Header
	$page_title = "Wifi | Log In";
	include 'include/header.php';

 ?>
<body class="bodylogin">
    <div class="bgimg-4">
        <?php include 'include/menu.php';?>

        <?php include 'include/login_form.php'; ?>
        <?php include 'include/footer.php';?>

    </div>  
</body>
</html>