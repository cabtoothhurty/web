//User location functions...
    function getLocationConstant() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(onGeoSuccess, onGeoError);

        } else {
            alert("Your browser or device doesn't support Geolocation");
        }
    }

    // If we have a successful location update
    function onGeoSuccess(event) {
        document.getElementById("Latitude").value = event.coords.latitude;
        document.getElementById("Longitude").value = event.coords.longitude;
        document.getElementById("Position1").value = event.coords.latitude + ", " + event.coords.longitude;

    }

    // If something has gone wrong with the geolocation request
    function onGeoError(event) {
        alert("Error code " + event.code + ". " + event.message);
    }


// Google map for results page
    function myMap() {
        var mapOptions = {
            center: new google.maps.LatLng(51.5, -0.12),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.HYBRID
        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    }


    var customLabel = {
        restaurant: {
        label: 'R'
        },
        bar: {
        label: 'B'
        }
    };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(-27.4850511, 152.9925091),
        zoom: 11
        });
        var infoWindow = new google.maps.InfoWindow;


        // Change this depending on the name of your PHP or XML file
        downloadUrl('http://localhost/results.xml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
                var id = markerElem.getAttribute('id');
                var name = markerElem.getAttribute('name');
                var address = markerElem.getAttribute('address');
                var type = markerElem.getAttribute('link');
                var point = new google.maps.LatLng(
                    parseFloat(markerElem.getAttribute('lat')),
                    parseFloat(markerElem.getAttribute('lng')));

                var infowincontent = document.createElement('div');
                var strong = document.createElement('strong');
                strong.textContent = name
                infowincontent.appendChild(strong);
                infowincontent.appendChild(document.createElement('br'));

                var text = document.createElement('text');
                text.textContent = address
                infowincontent.appendChild(text);
                infowincontent.appendChild(document.createElement('br'));


                var a = document.createElement('a');
                a.setAttribute('href', type);
                a.innerHTML = type;
                infowincontent.appendChild(a);


               /*  var linkText = document.createTextNode("my title text");
                text.appendChild(linkText);
                text.title = "my title text";
                text.href = type;
                document.body.appendChild(text); */

                var icon = customLabel[type] || {};

                var marker = new google.maps.Marker({
                    map: map,
                    position: point,
                    label: icon.label
                });
                
                marker.addListener('click', function() {
                    infoWindow.setContent(infowincontent);
                    infoWindow.open(map, marker);
                });
            });
        });
        }



    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
        if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
        }
        };

        request.open('GET', url, true);
        request.send(null);
    }

    function doNothing() {}


