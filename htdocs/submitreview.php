<?php
//Header
include 'include/main.php';

if (isset($_SESSION['User'])){
    include 'include/database_connection.php';    
    include 'include/database_library.php'; 
    
    //check user exists in database (to prevent hacking of submitreview.php page)
    $email = htmlspecialchars($_SESSION['User']);
    if (userExists($email, $pdo)){
        $review = htmlspecialchars(($_POST['Review']));
        $name = htmlspecialchars(($_POST['name']));
        //if star not set then user rated zero stars
        if (isset($_POST['star'])){
            $star = htmlspecialchars(($_POST['star']));
        } else {$star = 0;}

        ////////////////WiFi ID///////////////////
        $wifiID_string = getWifiID($name, $pdo);
        ////////////////User ID///////////////////
        $email = $_SESSION['User'];
        $userID_string = getUserID($email, $pdo);
        ////////////////Star Rating///////////////////
        //Update the listing rating to be the average of all ratings for listing. 
        $new_rating = calcNewRating($pdo,  $wifiID_string );
        //insert the newly calculated rating into the items entry. 
        insertNewRating($new_rating, $wifiID_string, $pdo);

        //insert all the relevant info into the database
        if(insertReview($userID_string, $star, $review, $wifiID_string, $pdo)){
            $reviewd = true;
            echo "<font color=\"black\">added review</font>";
            header("Location: http://localhost/individual.php?name=$name");
        } else {
            echo "Error: <br>" . $pdo->errorInfo();
        } 
    } else {
        header("Location: http://localhost/registration.php");
    }  
} else {
    header("Location: http://localhost/registration.php");
}
?>

