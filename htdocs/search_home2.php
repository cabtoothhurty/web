<!-- 
To do:

Modify floating text to be bold and white
Create Logo image on left
Create line underneath header. 
formalize all text in search bar
 -->

<!DOCTYPE html>
<html>
    
<head>

    <style>
        

    body, html {
    height: 100%;
    margin: 0;
    font: 400 15px/1.8 "Lato", sans-serif;
    color: #fff;
    }


    footer, {
        font: 400 15px/1.8 "Lato", sans-serif;
        color: #fff;
        text-align: center;
    }

    
    .footerheading {
        color: #fff;
        text-align: center;

    }
    .footerinfo {
        font: 400 15px/1.8 "Lato", sans-serif;
        color: #fff;
        text-align: center;

    }

    .layerfooter {
    background-color: rgb(0, 18, 46);
    position: absolute;

    opacity: 0.9;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
    }

    .bgimg-2 {
    background-image: url("people.jpg");
    height: 20%;
    }

    .bgimg-1, .bgimg-2, .bgimg-3 {
    position: relative;
    opacity: 0.8;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    }

    .bgimg-1 {
    background-image: url("people.jpg");
    height: 100%;
    }


	.layer {
    background-color: rgb(33, 48, 70);
    position: absolute;
    opacity: 0.7;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
    }

    .caption {
    float: left;
    left: 50%;
    margin-left: -470px;
    margin-top: 200px;
    position: relative;
    width: 940px;
    border-radius: 80px;
    background-color: #fff;
    }

  /*   .caption span.border {
    background-color: #111;
    color: #fff;
    padding: 18px;
    font-size: 25px;
    letter-spacing: 10px;
    float:center;
    } */

	.layer {
    background-color: rgb(33, 48, 70);
    position: absolute;
    opacity: 0.7;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    }

    h3 {
    letter-spacing: 5px;
    text-transform: uppercase;
    font: 20px "Lato", sans-serif;
    color: #111;
    }

    .parent {
        display: flex;
        /* justify-content: flex-start; */
        justify-content: center;
        align-items: center;
    }
    .menu {
    opacity: 1; 
    text-align:center;
    position: relative;
    display: inline-block;
    border-bottom: 1px solid white;

    }

    .menu a {
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 35px;
    }

    .menu a:hover {
    color: black;
    }

    .menu a.active {
    color: white;
    }

    /* Style the search box inside the center box */
    .caption input[type=text] {
        float: center;
        padding: 6px;
        border: none;
        margin-top: 8px;
        margin-right: 16px;
        font-size: 17px;
    }

    .rating {
    unicode-bidi: bidi-override;
    direction: rtl;
    display: table-cell;
    position: relative;
    vertical-align: middle;
    padding: 0px 0px;
    color: rgb(83, 95, 112);
    border: none;
    white-space: nowrap;
    text-align: center;
    }
    .rating > span {
    display: inline-block;
    position: relative;
    width: 1.1em;
    }
    .rating > span:hover:before,
    .rating > span:hover ~ span:before {
    content: "\2605";
    position: absolute;
    }

    .field {
    display: table-cell;
    position: relative;
    vertical-align: middle;
    padding: 0px 5px;
    font-family: Oswald;
    font-size: 17px;
    }

    .fieldsearch {
    background-color: #d90429;
    display: table-cell;
    position: relative;
    vertical-align: middle;
    padding: 0px 30px;
    border-top-right-radius: 80px;
    border-bottom-right-radius: 80px;

    }

    .search {
        background-color: #d90429;
        border: medium none;
        color: #ffffff;
        float: right;
        font-family: Oswald;
        font-size: 21px;
        height: 60px;
        margin-left: -1px;
        padding: 0 50px;
        width: 100%;
        border-radius: 0 30px 30px 0;

    }

    .selectbox {
        /* display: none; */
        border: medium none;
        /* padding: 4px 15px; */
        /* width: 100%; */
        float: right;
        font-family: Oswald;
        height: 60px;
        margin-left: -1px;
        padding: 0 50px;
        font-size: 18px;

    }

    .heading {
        color: #ffffff;
        float: center;
        font-family: Oswald;
        font-size: 30px; 
        z-index: 1000;

    }

    .heading2 {
        color: #ffffff;
        float: center;
        font-family: Oswald;
        font-size: 20px; 
        z-index: 1000;
    }

    .keywordinput {
        font-family: Oswald;
        font-size: 18px;
        margin-left: 10px;

    }

    .LocationButton{
        border: none;
        background: none;
        border: medium none;
        float: right;
        font-family: Oswald;
        height: 60px;
        margin-left: -1px;
        padding: 0 50px;
        width: 100%;
        font-size: 18px;
        white-space: nowrap;
        text-align: center;
    }

    </style>
    <script type="/text/javascript" src="javascript.js"></script>
</head>
<body>



    <div class="bgimg-1">
    
            <div class="layer">
            </div>
    
            <div class="parent">
                    <div class="menu" >
                            <a class="active" href="#home">WiFind</a>
                            <a href="#news">Listings</a>
                            <a href="#contact">Pages</a>
                            <a href="#about">Register</a>
                    </div>

                    <!-- <div class="acountbtn">
                            <span class="register">Register</span>
                    </div> -->
            </div>
    
        <div class="heading"; align="center";>
                <br><br><h1>Find the Best Wifi Spots</h1>
        </div>
        
        <div class="heading2" align="center">        
            <h2>All the wifi locations around Brisbane</h2>
        </div>



        <div class="caption";>

            <form action="http://localhost/results.php" method="post">
                <div class="field">
                    
                    <input type="text" name="name" class="keywordinput" placeholder="Keywords" size="10">
                </div>

                <div class="field">
                    <select name="Suburb" class="selectbox">
                        <option value="" disabled selected>Select your Suburb</option>
                        <option value="South Bank">South Bank</option>
                        <option value="Kangaroo Point">Kangaroo Point</option>
                        <option value="West End">West End</option>
                        <option value="City">City</option>
                    </select>
                </div>

                    <div class="rating">
                            <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                    </div>

                <div class="field">
                    <button class="LocationButton" onclick="getLocation()">My Location</button>

                    <p id="demo"></p>
                    
                    <script>
                    var x = document.getElementById("demo");
                    
                    function getLocation() {
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(showPosition);
                        } else { 
                            x.innerHTML = "Geolocation is not supported by this browser.";
                        }
                    }
                    
                    function showPosition(position) {
                        x.innerHTML = "Latitude: " + position.coords.latitude + 
                        "<br>Longitude: " + position.coords.longitude;
                    }
                    </script>
                </div>                    

                <div class="fieldsearch">
                    <button type="submit" name="Submit" class="search">SEARCH</button>
                </div>
            </form>

        </div>





    </div>
    
    
</body>

<footer>
        <div class="bgimg-2">
            <div class="layerfooter">
            </div>

            <div >
                <h3 class="footerheading">CONTACT INFORMATION</h3>
                <div class="footerinfo">
                    <ul>
                        <li style="list-style-type:none">
                                <font color="white">Proudly Made in Australia</font>
                        </li>
                        <li style="list-style-type:none">
                            
                                <font color="white">Cell Phone</font>
                                <font color="white">+0447445861</font>
                        </li>
                        <li style="list-style-type:none">
                                <font color="white">E-mail Address</font>
                            <a href="mailto:georgefitzgerald2718@gmail.com" >georgefitzgerald2718@gmail.com</a>
                        </li>
                    </ul>
                </div>

            <div >
                <font color="white">Copyright GeorgeAndLachy © 2018. All Rights Reserved</font>
            </div>

        </div>


</footer>



</html>