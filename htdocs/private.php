<?php
    /* if (!isset($_SESSION['isAdmin'])) {
    header("Location: http://{$_SERVER['HTTP_HOST']}/Login.php");
    exit();
    }
    echo "This is the private page only accessable by an admin...";
 */

?>


<!DOCTYPE html>
<html>

<head>
    <style>
        .wrapper {
        text-align:center;
        }

        .wrapper
        {
            display: flex;

            width: 100%; /*Optional*/
        }
        .Column
        {
            display: table-cell;
            width:50%;
        }

        .grid-container {    	
        display: grid;
        background-color: #F0FFFF;
        padding: 5px 5px;
        grid-gap: 30px;
        grid-template-columns: 80px 100px auto 40px;
        }
        
        .left {
        width: 70%;
        }

        .right {
        width: 30%;
        }
        
        .google-map{
        }


    </style>

</head>

<body>
    <div class = "wrapper">
        <div class="Column">
            <div class="grid-container">
                <div class="grid-header"><b></b></div>
                <div class="grid-header"><b>HotSpot Name</b></div>
                <div class="grid-header"><b>Address</b></div>
                <div class="grid-header"><b>Suburb</b></div>


                <div class="grid-results">You haven't Searched for any wifi locations, but here is your recommended spot.</div>
                <div class="grid-results"><b><img src="hamilton.jpg" alt="Hamilton Street" style="width:100px;height:100px;"></b></div>
                <div class="grid-results"><a href="http://localhost/individual.php?name=Chermside Library Wifi"> Chermside Library Wifi</a></div>
                <div class="grid-results">375 Hamilton  Road</div>

                <div class="grid-results">You haven't Searched for any wifi locations, but here is your recommended spot.</div>
                <div class="grid-results"><b><img src="hamilton.jpg" alt="Hamilton Street" style="width:100px;height:100px;"></b></div>
                <div class="grid-results"><a href="http://localhost/individual.php?name=Chermside Library Wifi"> Chermside Library Wifi</a></div>
                <div class="grid-results">375 Hamilton  Road</div>

            </div>
        </div>
        <div class="Column">
            <div id="google-map">
                <iframe src="https://www.google.com/maps/d/embed?mid=1Rlo2Er2meqYRPpwMHi1K6AYi1za5wKUL" 
                width="200" height="300"></iframe> 
            </div>
        </div>



                        
    </div>
</body>
<html>



    
	.grid-container {
        
        display: grid;
        grid-template-columns: 80px 100px auto 40px;
        background-color: #F0FFFF;
        padding: 5px 5px;
        grid-gap: 30px;

	
    }
        .grid-header {
        background-color: #F0FFFF;
        border: 1px solid  #fff;
        padding: 1px;
        font-size: 25px;
        color: #000000;
        text-align: center;
    }

        .grid-results {
        background-color: #F0FFFF;
        border: 1px solid  #fff;
        padding: 1px;
        font-size: 20px;
        color: #000000;
        text-align: center;
    }

        #google-map {
        margin-right: 17.5%;
        margin-left: 62%;
        display: inline-block;
        
        
            
        }
        
        .wrapper {
        display:flex;
        text-align:left;
        vertical-align:top;
        font-size:16px;
        width: 75%; /*Optional*/

        .map {
        }
        

                <div class = "wrapper">
            <div class = "column left"
                <div class="grid-container">
                    <div class="grid-header"><b></b></div>
                    <div class="grid-header"><b>HotSpot Name</b></div>
                    <div class="grid-header"><b>Address</b></div>
                    <div class="grid-header"><b>Suburb</b></div>

                <?php
                    $pdo = new PDO('mysql:host=localhost;dbname=wifind', 'admin3', '1');
                    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    
                    //add connect error in connection.php

                    if (isset($_POST['name']) ){
                        $name = $_POST['name'];

                        $result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address` FROM `items` WHERE `WifiName` LIKE :name");

                        if (!$result->execute(array(':name' => $name)))
                        {
                            echo "failed to find the any results for given name";
                        }
                    }

                    if (isset($_POST['star']) ){
                        $star = $_POST['star'];

                        //get all locations with the specified star rating

                        $result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address` FROM `items` WHERE `Rating` = :star");
                        
                        if (!$result->execute(array(':star' => $star)))
                        {
                            echo "<div class=\"grid-results\">failed to find the result for star</div>";
                        }
                    }        
                    //echo "<div class=\"grid-results\">".(var_dump($_POST))."</div>";

                    if (isset($_POST['Latitude']) ){
                        $lat =$_POST['Latitude'];
                        if (isset($_POST['Longitude']) ){
                            $long =$_POST['Longitude'];


                            $lowlat = (string)((float)$lat + 0.01);
                            $uplat = (string)((float)$lat - 0.01);

                            $lowlong = (string)((float)$long - 1);
                            $uplong = (string)((float)$long + 1);
                            // create the range of latitude and longitude points to check 
                            //SELECT `WifiID`, `Suburb`, `Address` FROM `items` WHERE `Latitude` BETWEEN :lowlat AND :uplat AND `Longitude` BETWEEN :lowlong AND :uplong 
                            $result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address` FROM `items` WHERE `Latitude` BETWEEN :lowlat AND :uplat AND `Longitude` BETWEEN :lowlong AND :uplong");
                            if (!$result->execute(array(':lowlat' => $lowlat, ':uplat' => $uplat, ':lowlong' => $lowlong, ':uplong' => $uplong )))
                            {
                                echo "<div class=\"grid-results\">failed to find the result for suburb</div>";
                            }                        
                        }
                    }
        
                    if (isset($_POST['suburb']) ){
                        $sub = $_POST['suburb'];
                        $result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address` FROM `items` WHERE `Suburb` = :star");
                        if (!$result->execute(array(':star' => $sub)))
                        {
                            echo "<div class=\"grid-results\">failed to find the result for suburb</div>";
                        }
                    } 
                    if (($result->rowCount()) > 0){
                    $rows = $result->fetchAll(); 
                    foreach($rows as $row){
                        echo '<div class="grid-results"><b><img src="delaware.jpg" alt="Delaware Street" style="width:100px;height:100px;"></b></div>';
                        
                        //make wifi name hyperlink to indivdual page and submit name as get request
                        echo '<a href="http://localhost/individual.php?name='.$row[0].'">'.$row[0].'</a>';
                        //echo '<div class="grid-results">'.$row[0].'</div>';
                        echo '<div class="grid-results">'.$row[2].'</div>';
                        echo '<div class="grid-results">'.$row[1].'</div>';
                        } 
                    }  else { 
                        //display a message for an empty search but still list some alternatives
                        $result = NULL;

                        echo "<div class=\"grid-results\">You haven't Searched for any wifi locations, but here is your recommended spot.</div>";
                        echo"
                        <div class=\"grid-results\"><b><img src=\"hamilton.jpg\" alt=\"Hamilton Street\" style=\"width:100px;height:100px;\"></b></div>
                        <div class=\"grid-results\"><a href=\"http://localhost/individual.php?name=Chermside Library Wifi\"Chermside Library Wifi</a></div>
                        <div class=\"grid-results\">375 Hamilton  Road</div>";
                    }  
                ?>

                </div>
            </div>

            <div class = "column right"

                <div id="google-map">
                    <iframe src="https://www.google.com/maps/d/embed?mid=1Rlo2Er2meqYRPpwMHi1K6AYi1za5wKUL" 
                    width="500" height="347"></iframe> 
                </div>
            </div>