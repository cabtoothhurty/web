<?php
//parse through a  csv and relay data
$row = 1;
if (($handle = fopen("scrubbed.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        //echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        for ($c=0; $c < $num; $c++) {
            //echo $data[$c] . "<br />\n";
        } 
    }
    fclose($handle);
}

//make sure LL_functions.php is in same folder as this file. 
include 'LL_functions.php';

//parse csv into an array 
// array str_getcsv ( string $input [, string $delimiter = "," [, string $enclosure = '"' [, string $escape = "\\" ]]] )
$csv = array_map('str_getcsv', file('scrubbed.csv'));

//get the name of the first student in the csv file - we're only storing their results
$hash = $csv[2][0];
echo $hash;

//run 
loadOneUsersEchoTimes($hash);

function loadOneUsersEchoTimes($hash){
    $csv = array_map('str_getcsv', file('scrubbed.csv'));

    foreach($csv as $row){
        //echo var_dump($row[3]);
        $duration = (int)$row[4];

        if ($hash == $row[0]){
            //insert data into learning locker
            InsertEchoTimeStatement($hash, $duration);
        } 
    }
}
?> 



