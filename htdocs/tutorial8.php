<?php

$months = array('None' => 'Select...', 1 => 'Jan', 2 => 'Feb', 3 => 'Mar',
4 =>'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug',
9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');

$errors = array();
if (isset($_POST['email'])) {
validateEmail($errors, $_POST, 'email');
if ($errors) {
echo '<h1>Invalid, correct the following errors:</h1>';
foreach ($errors as $field => $error) {
echo "$field $error<br>";
}
// redisplay the form
include 'T8_form.php';
select('month', $months);

} else {
echo 'form submitted successfully with no errors';
include 'T8_form.php';
select('month', $months);
}
} else {
include 'T8_form.php';
}


function validateEmail(&$errors, $field_list, $field_name) {
    $pattern = '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/';
    if (!isset($field_list[$field_name])|| empty($field_list[$field_name])) {
    $errors[$field_name] = 'required';
    } else if (!preg_match($pattern, $field_list[$field_name])) {
    $errors[$field_name] = 'invalid';
    }
    }

function input_field($errors, $name, $label) {
    echo '<div class="required_field">';
    label($name, $label);
    $value = posted_value($name);
    echo "<input type=\"text\" id=\"$name\" name=\"$name\" value=\"$value\"/>";
    errorLabel($errors, $name);
    echo '</div>';
}

function select($name, $values) {
    echo "<select id=\"$name\" name=\"$name\">";
    foreach ($values as $value => $display) {
    $selected = ($value==posted_value($name))?'selected="selected"':'';
    echo "<option $selected value=\"$value\">$display</option>";
    }
    echo '</select>';
    }



?>