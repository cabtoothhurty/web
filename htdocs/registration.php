
<!DOCTYPE html>
<html>
<?php    
//Header
	$page_title = "Wifi | Register";
    include 'include/header.php';
?>
<body>
        <div class="bgimg-1">
                <div class="layer">
                </div>
                <?php include 'include/menu.php';?>
            <div class="modal">
                <form class="signupcotainer" action="http://localhost/login.php" method="post" >
                    <div class="container">
                        <h2 class="signup" >Sign Up</h2>
                        <font color="black">Please fill in this form to create an account.</font>
                        <hr>
                        <label for="email"><b>Email</b></label>
                        <input type="email" placeholder="Enter Email" name="email" required>

                        <label for="mobile"><b>Mobile</b></label>
                        <input type="number" placeholder="Enter mobile" name="mobile" required>
                    
                        <label for="birthday"><b>Birthday</b></label>
                        <input type="date" placeholder="Enter Birthday" name="birthday" required>

                        <label for="psw"><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="psw" required>
                    
                        <label for="psw-repeat"><b>Repeat Password</b></label>
                        <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
                        
                        <label>
                            <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
                        </label>
                        <br>
                        <p><font color="black">By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</font>.</p>
                        <div class="clearfix">
                            <button type="submit" class="signupbtn">Sign Up</button>
                        </div>
                    </div>
                </form>
            </div>
    </div>
</body>
<?php
	include 'include/footer.php';
?>    
</html>