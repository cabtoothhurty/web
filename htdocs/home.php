
<!DOCTYPE html>

<html>
<?php 
    session_start();

    include 'include/database_connection.php';
    //Header
	$page_title = "Wifi | Home";
	include 'include/header.php';
?>

<body>

    <div class="bgimg-1">
    
        <div class="layer">
        </div>

        <?php
            include 'include/menu_home.php';
        ?>
    
        <div class="heading col-xs-12"; align="center";>
                <br><br><h1>Find the Best Wifi Spots</h1>
        </div>
        
        <div class="heading2 col-xs-12" align="center">        
            <h2>All the wifi locations around Brisbane</h2>
        </div>


        <div class="captionsearch col-xs-12";>

            <form action="http://localhost/results.php" method="post">
                <div class="fieldtext">
                    <input type="text" name="name" class="keywordinput" placeholder="Keywords">
                </div>

                <div class="field">
                    <select name="suburb" class="selectbox">
                    <option value="" disabled selected>Select your Suburb</option>

                        <?php
                            include 'include/database_connection.php';
                            include 'include/database_library.php';

                            $allsubs = getAllSuburbs($pdo);

                            $suburbs = $allsubs->fetchAll();

                            //list results as options
                            foreach ($suburbs as $suburb) {
                                echo "<option name=\"suburb\" value=\"".$suburb[0]."\">" . ucwords(strtolower($suburb[0])) . "</option>";
                            }
                        ?>
                        <option name="suburb" value="Chermside">YaMum</option>
                    </select>
                </div>

                <?php
                    include 'include/star_rating2.php';
                ?>

                <div class="field" name="location">
                    <input class="LocationButton" type="button" value="My Location" onclick="getLocationConstant()" /> 
                    <p id="demo"></p>
                        <input id="Latitude" type="hidden" name="Latitude" value="">
                        <input id="Longitude" type="hidden" name="Longitude" value="">
                </div>                    

                <div class="fieldsearch">
                    <button type="submit" name="Submit"  class="search">SEARCH</button>
                </div>
            </form>

        </div>
 
    </div>
    <?php
	    include 'include/footer.php';
    ?>
    
    <script type="text/javascript" src="javascript/main.js"></script>

</body>
</html>