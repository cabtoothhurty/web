<!DOCTYPE html>
<html>

<?php 
//Code to register a user

    //delete this in final project, if all pages are accessed from home page. 
    session_start();

    //Define Global Error Variables
    $_SESSION['registered']=TRUE;
    $_SESSION['username'] = $_SESSION['password'] = "";
    $_SESSION['username_err'] = $_SESSION['password_err'] = "";
    //connect to database and get access to query functions.
    include 'include/database_connection.php';    
    include 'include/database_library.php'; 
    include 'include/main.php';

    //Register a user if the post contains all registration data
    $_SESSION['registered'] = registerUser($_POST, $pdo);

    // Processing form data when log in is submitted
    if($success = loginUser($_POST, $pdo)){
        $_SESSION['registered'] = true;
        $_SESSION['re_password']=FALSE;
        $_SESSION['re_email']=FALSE;
    }
    
    //If user is trying to log in without registration
    if (count($_POST) == 0){
        $_SESSION['registered'] = false;
        $_SESSION['re_password']=FALSE;
        $_SESSION['re_email']=FALSE;
    }

    if (count($_POST) == 2){
        $_SESSION['registered'] = true;
    }

    //Header
	$page_title = "Wifi | Log In";
	include 'include/header.php';

 ?>
<body class="bodylogin">
    <div class="bgimg-3">
<!--         <div class="layer">
        </div> -->
        <?php include 'include/menu.php';?>
        <?php if (!$_SESSION['registered']) : ?>
            <?php include 'include/rego_form.php'; ?>
        <?php else : ?>
            <?php include 'include/login_form.php'; ?>
        <?php endif; ?>
        
        <?php include 'include/footer.php';?>

    </div>  
</body>
</html>