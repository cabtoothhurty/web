<?php

    function displayItem($result, $pdo){ 
        $row = $result->fetch(); 
        echo '<h3>'.$row[0].'</h3>';
        echo '<div class="stars"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';
        //print the number of stars as retrieved from the database 
        for($i = 1; $i <= $row[1]; $i++){
            echo '<span class="fa fa-star checked"></span>';
        }                    
        //print blank stars

        for($i = (5-$row[1]); $i > 0;$i--){
            echo '<span class="fa fa-star"></span>';
        }
        echo '</div>';
    }


    function registerUser($POST, $pdo){
        
        //check if register form was submitted
        if (count($POST) == 6) {

            //reset variables used to keep track of which error user encounted in last registration attempt
            $re_password=FALSE;
            $re_email=FALSE;
            $try_registration = TRUE;

            //Get form data while prevent XSS and SQl injections.
            $email = htmlspecialchars(($_POST['email']));
            $mobile = htmlspecialchars(($_POST['mobile']));
            $bday = htmlspecialchars(($_POST['birthday']));
            $psw = htmlspecialchars(($_POST['psw']));
            $pswR = htmlspecialchars(($_POST['psw-repeat']));

            //vaidate input
            if (invalidRegForm($_POST)){
                $try_registration = false;
            }

            //hash the password and username from email
            //$hashed = hash('sha512', $psw);
            $hashed = password_hash($psw, PASSWORD_DEFAULT);
            $remember = $_POST['remember'];
            $userid = rand();


            
            //Check if the username exists in the database
            if(checkUserNameUnique($POST, $pdo)){
                $registered = false;
                $re_email = TRUE;
                $try_registration = false;
                echo  "<script>alert(\"username exists\");</script>";
            }

            //check if the passwords match
            if(!passwordsMatch($POST)){
                echo  "<script>alert(\"Passwords Didn't Match\");</script>";
                $try_registration = false;
            }

            if ($try_registration) {
                if ($worked = insertUser($userid, $bday, $hashed, $email, $pdo)){
                    return true;
                } else {return false;}
            } else return false;
        }
    }

    //returns true if the user is not unique
    function checkUserNameUnique($POST, $pdo){

        $email = htmlspecialchars(($POST['email']));
        $DBpassword = $pdo->query("SELECT `Email` FROM `members` WHERE `Email` LIKE '$email' ");

        if (($DBpassword->rowCount()) > 0){
            return TRUE;
        }
    }

    function passwordsMatch($POST){
        $psw = htmlspecialchars(($_POST['psw']));
        $pswR = htmlspecialchars(($_POST['psw-repeat']));

        if ($psw != $pswR){
            $registered = false;
            $re_password = TRUE;
            $try_registration = false;
            return FALSE;
        } else {return TRUE;}
    }

    function invalidRegForm($POST){
        $email = htmlspecialchars(($POST['email']));
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $mobile = htmlspecialchars(($POST['mobile']));
            if (preg_match('/^[0-9]{10}+$/', $mobile)){
                $bday = htmlspecialchars(($POST['birthday']));
                if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $bday)){
                    $psw = htmlspecialchars(($POST['psw']));
                    if (preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $psw)){
                        return false;
                    } else {
                        echo  "<script>alert(\"The password you must contain letters and numebrs.\");</script>";
                        return true;
                    }
                }else {
                    echo  "<script>alert(\"The birthday must be of correct format.\");</script>";
                    return true;
                }
            } else {
                echo  "<script>alert(\"Your mobile number must be a number and 10 digits long.\");</script>";
                return true;
            }
        } else {
            echo  "<script>alert(\"Your email must be valid contain letters or numbers, @ some address.\");</script>";
            return true;
        }
    }

    function loginUser($POST, $pdo2){
        if(count($_POST) == 2){
            $username = $password = "";
            $username_err = $password_err = "";
             // Check if username is empty
             if(empty(trim($_POST['email']))){
                 $username_err = 'Please enter email.';
             } else{
                 $username = htmlspecialchars((trim($_POST["email"])));
             }
             
             // Check if password is empty
             if(empty(trim($_POST['password']))){
                 $password_err = 'Please enter your password.';
             } else{
                 $password = htmlspecialchars((trim($_POST['password'])));
             }
             
             // Validate credentials
             validateCred($username_err, $password_err, $_POST, $pdo2, $password, $username);
    
        }
    }


    function validateCred($username_err, $password_err, $POST, $pdo2, $password, $username){

        if(empty($username_err) && empty($password_err)){   
                
            $DBpassword = getDBPassword($POST, $pdo2);     

            //Hash the entered password before checking
            //$hashedP = hash('sha512', $password);
            //$hashedP = password_hash($password, PASSWORD_DEFAULT);

            
            //check if username exists
            if(isset($DBpassword) ){
                //check that password matches
                if (password_verify($password, $DBpassword)){
                    //start new session with username
                    $_SESSION['User'] = $username;
                    $_SESSION['IsAdmin'] = TRUE;
                    $registered = true;
                    $re_password=FALSE;
                    $re_email=FALSE;
                    header("Location: http://localhost/home.php");
                    return true;        
                } else {
                    // Display an error message if password is not valid
                    $password_err = 'The password you entered was not valid.';
                    echo  "<script>alert(\"The password you entered was not valid.\");</script>";
                    return $password_err;
                }
            } else{
                // Display an error message if username doesn't exist
                $username_err = 'No account found with that username.';
                echo  "<script>alert(\"The email you entered was not found.\");</script>";
                return $username_err;
            } 
        }        
    }

    function calcNewRating($pdo,  $wifiID_string){

        $ratings = getRatings($wifiID_string, $pdo);
        //$vardump = var_dump($ratings);
        //echo  "<script>alert(\"query db for rating\");</script>";

/*         echo '<br>Vardump Ratings <br>';
        echo $vardump; */
        //calculate the new star rating based on average rounded 
        $total = 0;
/*         if (!isset($ratings) ){
            $new_rating= 0;
            return $new_rating;
        } else {  */   
        if ($ratings->rowCount() > 0){
            foreach($ratings as $row){
                $total = $total + (int)$row[0];
            }
            $numberratings = $ratings->rowCount();
            $average = $total / $numberratings;
            $new_rating = round($average);
            echo  "<script>alert(\"Rating returned calc avg\");</script>";

            return $new_rating;
        } else {
            return false;
        }

        /* } */

    }



function noResults($pdo){
    $sub = "Chermside";
    $result = getAllBySuburb($sub, $pdo);                            
    createXML($result); 
    $result = getAllBySuburb($sub, $pdo);                            
    echo '<div class="grid-results"></div>';
    //make wifi name hyperlink to indivdual page and submit name as get request
    echo '<div class="grid-results"></div>';
    echo '<div class="grid-results">No results found for your search. Here are our suggestions:</div>';
    echo '<div class="grid-results"></div>';
    $rows = $result->fetchAll(); 
    if (count($rows) > 0){
        foreach($rows as $row){
            echo '<div class="grid-results"><b><img class="result-img" src="delaware.jpg" alt="Delaware Street"></b></div>';
            //make wifi name hyperlink to indivdual page and submit name as get request
            echo '<div class="grid-results"><a href="http://localhost/individual.php?name='.$row[0].'">'.$row[0].'</a></div>';
            echo '<div class="grid-results">'.$row[2].'</div>';
            echo '<div class="grid-results">'.$row[1].'</div>';
        }
    }  
}
?>