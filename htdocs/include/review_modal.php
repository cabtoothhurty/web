
<?php    
//Header
    include 'include/main.php';    

    //Globals 
    $userLoggedIn;

    //Check that the session user exists in the database... 
    if (isset($_SESSION['User'])){
        include 'include/database_connection.php';    
        //include 'include/database_library.php'; 

        $email = htmlspecialchars($_SESSION['User']);

        if (userExists($email, $pdo)){
            $userLoggedIn = true;
            //display the modal form
            echo "<div class=\"modalreview\">";
            // <div class=\"modal-content\"";
            include 'include/review_form.php';
            echo "</div>";//</div>";
        } else {
            echo  "<script>alert(\"You need to be logged in to submit a review. Not a member? Sign up on our Register Page.\");</script>";
            //header("Location: http://localhost/register.php");
        }
    } else {
        echo  "<script>alert(\"You need to be logged in to submit a review. Not a member? Sign up on our Register Page.\");</script>";
        //header("Location: http://localhost/register.php");
    }
?>

