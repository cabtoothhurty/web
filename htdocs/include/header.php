<head>
		

	<!-- Mobile metadata -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<title><?php echo $page_title; ?></title>
	<link rel="manifest" href="manifest.json">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="apple-touch-icon" href="img/wifind_logo2.png">
	<meta name="application-name" content="WifiLocations">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	<link rel="icon" type="image/x-icon" href="favicon.ico" />

	<link rel="stylesheet" type="text/css" href="CSS/style.css" />
</head>