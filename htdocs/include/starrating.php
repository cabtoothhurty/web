<div class="rating">
    <input id="star5" name="star" type="radio" value="5" class="radio-btnhide" />
    <label for="star5" >&#9734</label>
    <input id="star4" name="star" type="radio" value="4" class="radio-btnhide" />
    <label for="star4" >&#9734</label>
    <input id="star3" name="star" type="radio" value="3" class="radio-btnhide" />
    <label for="star3" >&#9734</label>
    <input id="star2" name="star" type="radio" value="2" class="radio-btnhide" />
    <label for="star2" >&#9734</label>
    <input id="star1" name="star" type="radio" value="1" class="radio-btnhide" />
    <label for="star1" >&#9734</label>
    <div class="clear"></div>
</div>