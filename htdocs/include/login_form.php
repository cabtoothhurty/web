<?php

    //Set session error variables to determine which html to display
    if(count($_POST) == 6){
        if(checkUserNameUnique($_POST, $pdo)){
            $_SESSION['registered'] = false;
            $_SESSION['re_email'] = TRUE;
            $try_registration = false;
        }
        if(!passwordsMatch($_POST)){
            $_SESSION['registered'] = false;
            $_SESSION['re_password'] = TRUE;
            $try_registration = false;
        }
    }
?>

<div class="modal">
    <form class="signupcotainer" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="containerlogin">
            <h2 class="signup" >Log In</h2>
            <?php
                if ( $_SESSION['registered']){
                    echo  '<font color="black">Enter Password to Log in</font>';
                }
                
                if (isset($_POST['email'])){
                    if(checkUserNameUnique($_POST, $pdo)){
                        $email = $_POST['email'];
                    } else {
                        $email = "Please enter a different email.";
                        $_SESSION['username_err'] = "Error";
                    }
                } else {$email = "";}

                if (isset($_POST['password'])){
                    $password = $_POST['password'];
                    $_SESSION['password_err'] = "";
                    $DBpassword = getDBPassword($_POST, $pdo);     
                    $hashedP = hash('sha512', $password);
                    if ($hashedP == $DBpassword){
                        $password = $_POST['password'];
                    } else {$password = "";}
                } else {$password = "";}
                
            ?>
            <div>
                <label>Email</label>
                <input type="text" name="email" value="<?php echo $email; ?>">
                <span class="help-block"><?php echo $_SESSION['username_err']; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($_SESSION['password_err'])) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="help-block"><?php echo $_SESSION['password_err']; ?></span>
            </div>

            <div class="clearfix">
                <button type="submit" value="Login" class="signupbtn">Log In</button>                            
            </div>                
            <p><font color="black">Don't have an account?</font> <a href="register.php">Sign up now</a>.</p>
        </div>
    </form>
</div>
