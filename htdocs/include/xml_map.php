<?php

//require("phpsqlajax_dbinfo.php");

function parseToXML($htmlStr)
{
$xmlStr=str_replace('<','&lt;',$htmlStr);
$xmlStr=str_replace('>','&gt;',$xmlStr);
$xmlStr=str_replace('"','&quot;',$xmlStr);
$xmlStr=str_replace("'",'&#39;',$xmlStr);
$xmlStr=str_replace("&",'&amp;',$xmlStr);
return $xmlStr;
}

//test this function
/* include 'include/database_connection.php';
include 'include/database_library.php';
$locations = getAllMaps($pdo);
createXML($locations); */

//inserts location data into an xml file (results.xml) for google maps
function createXML($locations){
    $writer = new XMLWriter();  
    $writer->openURI('results.xml');  
    $writer->startDocument('1.0','UTF-8');  
    $writer->setIndent(2);   
    $writer->startElement('markers');  

        // Iterate through the rows, printing XML nodes for each
        foreach ($locations as $row){
            $writer->startElement('marker');
                $writer->writeAttribute('id', $row['WifiID']);  
                $writer->writeAttribute('name', $row['WifiName']); 
                $writer->writeAttribute('address', $row['Address']); 
                $writer->writeAttribute('lat', $row['Latitude']); 
                $writer->writeAttribute('lng', $row['Longitude']); 
                $link = 'http://localhost/individual.php?name='.$row['WifiName'];
                $writer->writeAttribute('link', $link); 
            $writer->endElement();  
        }
    
    $writer->endElement();  
    $writer->endDocument();   
    $writer->flush();

}
?>