<?php
//Header
include 'include/main.php';

//check post variables are present.


if (isset($_SESSION['User']) && isset($_POST['Review'])){
    include 'include/database_connection.php';    
    echo  "<script>alert(\"User set and Review set\");</script>";

    //check user exists in database (to prevent hacking of submitreview.php page)
    $email = htmlspecialchars($_SESSION['User']);
    if (userExists($email, $pdo)){
        $review = htmlspecialchars(($_POST['Review']));

        $name = htmlspecialchars(($_POST['name']));

        //validate name and review text
        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $name))
            {
                echo  "<script>alert(\"Special Characters not permitted in POST name.\");</script>";
                //header("Location: http://localhost/individual.php?name=$name&review=true");
            }

        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $review))
            {
                echo  "<script>alert(\"Special Characters not permitted in review.\");</script>";
                //header("Location: http://localhost/individual.php?name=$name&review=true");
            }

        
        //if star not set then user rated zero stars
        if (isset($_POST['star'])){
            $star = htmlspecialchars(($_POST['star']));
            if ((float)$star > 5 || (float)$star < 0){
                echo  "<script>alert(\"Star rating has to be integer values between 0 and 5.\");</script>";
            }
        } else {$star = 0;}

        $wifiID_string = getWifiID($name, $pdo);
        $email = $_SESSION['User'];
        $userID_string = getUserID($email, $pdo);

        //insert all the relevant info into the database
        if(insertReview($userID_string, $star, $review, $wifiID_string, $pdo)){
            $reviewd = true;
            echo  "<script>alert(\"Added Review.\");</script>";

            //Update the listing rating to be the average of all ratings for listing. 
            $new_rating = calcNewRating($pdo,  $wifiID_string );

            //insert the newly calculated rating into the items entry only if  review has been added. 
            $insertresult = insertNewRating($new_rating, $wifiID_string, $pdo);

            header("Location: http://localhost/individual.php?name=$name");
        } else {
            echo "Error: <br>" . $pdo->errorInfo();
        } 
    } else {
        header("Location: http://localhost/register.php");
    }  
} else {
    header("Location: http://localhost/register.php");
}
?>

