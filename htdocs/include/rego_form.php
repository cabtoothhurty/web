<?php
    //Set variables to determine which html to display
    if(count($_POST) == 6){
        if(checkUserNameUnique($_POST, $pdo)){
            $_SESSION['registered'] = false;
            $_SESSION['re_email'] = TRUE;
        }
        if(!passwordsMatch($_POST)){
            $_SESSION['registered'] = false;
            $_SESSION['re_password'] = TRUE;
        }
        $try_registration = false;
    }

    if(count($_POST) == 0){
        $try_registration = true;
    }
?>

<div class="modal">
    <form class="signupcotainer" action="http://localhost/register.php" method="post" >
        <div class="containerlogin">
            <h2 class="signup" >Sign Up</h2>
            <font color="black">Please fill in this form to create an account.</font>
            <hr>
            <label for="email"><b>Email</b></label>
            
            <?php if ($try_registration) : //display the unfilled registration form so user can try to register?>
                <input type="email" placeholder="Enter Email" name="email" required>

                <label for="mobile"><b>Mobile</b></label>
                <input type="number" placeholder="Enter mobile" name="mobile" required>
            
                <label for="birthday"><b>Birthday</b></label>
                <input type="date" placeholder="Enter Birthday" name="birthday" required>

                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" required>
            
                <label for="psw-repeat"><b>Repeat Password</b></label>
                <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
                
                <label>
                    <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
                </label>

            <?php else : //form is redisplayed to user with old details. ?>

                <?php if ($_SESSION['re_email']) : //conditionally change email field based on whether it was incorrect?>
                    <input type="email" placeholder="Try A Different Email" name="email" required>
                <?php else : ?>
                    <input type="email" placeholder="Enter Email" name="email" value="<?php echo $_POST['email'];?>" required>
                <?php endif ; ?>

                <label for="mobile"><b>Mobile</b></label>
                <input type="number" placeholder="Enter mobile" name="mobile" value="<?php echo $_POST['mobile'];?>" required>
            
                <label for="birthday"><b>Birthday</b></label>
                <input type="date" placeholder="Enter Birthday" name="birthday" value="<?php echo $_POST['birthday'];?>" required>


                <label for="psw"><b>Password</b></label>
                <?php if ($_SESSION['re_password']) : //conditionally change password field based on whether it was incorrect?>
                    <input type="password" placeholder="Enter Password" name="psw" required>
                    <label for="psw-repeat"><b>Repeat Password</b></label>
                    <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
                
                <?php else : ?>
                    <input type="password" name="psw" value="<?php echo $_POST['psw'];?>" required>
                    <label for="psw-repeat"><b>Repeat Password</b></label>
                    <input type="password" name="psw-repeat" value="<?php echo $_POST['psw-repeat'];?>" required>
                
                <?php endif ; ?>
            

                <label>
                    <input type="checkbox" checked="checked" name="remember" value="<?php echo $_POST['remember'];?>" style="margin-bottom:15px"> Remember me
                </label>

            <?php endif ; ?>

            <br>
            <p><font color="black">By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</font>.</p>
        
            <div class="clearfix">
                <button type="submit" class="signupbtn">Sign Up</button>
            </div>
            </div>  
    </form>
</div>