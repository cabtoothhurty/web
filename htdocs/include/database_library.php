<?php

define("ERROR", "error");

    /* Start database Functions */
    
	function getAllSuburbs($pdo){
		try{
            $result = $pdo->prepare("SELECT DISTINCT `Suburb` FROM `items`");
            if (!$result->execute()){
                return ERROR;
            } else {return $result;}
		} catch (PDOException $ex) {
            return ERROR;
		}
    }

    function getAllMaps($pdo){
		try{
            $result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address`, `Latitude`, `Longitude`, `WifiID` FROM `items`");
            if (!$result->execute()){
                return ERROR;
            } else {return $result;}
		} catch (PDOException $ex) {
            return ERROR;
		}
    }

/*     SELECT * FROM MyTable
    WHERE CHARINDEX('word1', Column1) > 0
      AND CHARINDEX('word2', Column1) > 0
      AND CHARINDEX('word3', Column1) > 0
 */

	function getAllByNAme($name, $pdo){
		try{
            $result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address`, `Latitude`, `Longitude`, `Rating`, `WifiID` FROM `items` WHERE `WifiName` = :name");
            if (!$result->execute(array(':name' => $name))){
                return ERROR;
            } else {return $result;}
		} catch (PDOException $ex) {
            return ERROR;
		}
    }
    
    function getAllByStar($star, $pdo){
        try{
            $result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address`, `Latitude`, `Longitude`, `WifiID` FROM `items` WHERE `Rating` = :star");             
            if (!$result->execute(array(':star' => $star))){
                return ERROR;
            } else {return $result;}
        } catch (PDOException $ex) {
            return ERROR;
		}
    }
    
	function getAllByLocation($lat, $long, $pdo) {
		try {
			$lowlat = (string)((float)$lat + 0.01);
            $uplat = (string)((float)$lat - 0.01);
            $lowlong = (string)((float)$long - 1);
            $uplong = (string)((float)$long + 1);
            $result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address`, `Latitude`, `Longitude`, `WifiID` FROM `items` WHERE `Latitude` BETWEEN :lowlat AND :uplat AND `Longitude` BETWEEN :lowlong AND :uplong");
            if (!$result->execute(array(':lowlat' => $lowlat, ':uplat' => $uplat, ':lowlong' => $lowlong, ':uplong' => $uplong ))){
                return ERROR;            
            } 
            else {
                return $result;}       
		} catch (PDOException $ex) {
			return ERROR;
		}
	}

	function getAllBySuburb($sub, $pdo) {
		try {
			$result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address`, `Latitude`, `Longitude`, `WifiID` FROM `items` WHERE `Suburb` = :star");
            if (!$result->execute(array(':star' => $sub)))
            {
                return ERROR;
            } else {
            return $result;
            }
		} catch (PDOException $ex) {
			return ERROR;
		}
	}

    //function for individual item page. 
    function getOneByName($name, $pdo){
		try{
            $result = $pdo->prepare("SELECT `WifiName`, `Suburb`, `Address`, `Latitude`, `Longitude`, `WifiID` FROM `items` WHERE `WifiName` = :name");
            if (!$result->execute(array(':name' => $name))){
                return ERROR;
            } else {return $result;}
		} catch (PDOException $ex) {
            return ERROR;
		}
    }

    //Get Description 
    function getDescription($name, $pdo){
		try{
            $result = $pdo->prepare("SELECT  `Description`  FROM `items` WHERE `WifiName` = :value");
            if(!$result->execute(array(':value' => $name))){
                return ERROR;
            } else {
                $row = $result->fetch(); 

                return $row;}
		} catch (PDOException $ex) {
            return ERROR;
		}
    }


    //Get the Reviews 
    function getReviews($name, $pdo){
		try{

            $wifiID = $pdo->prepare("SELECT  `WifiID` FROM `items` WHERE `WifiName` = :value");
            if(!$wifiID->execute(array(':value' => $name))){
                return NULL;
            }  
            $row = $wifiID->fetch(); 
            $wifiID = $row[0];
            $reviews = $pdo->prepare("SELECT  `ReviewText`, `Rating`, `Date`  FROM `reviews` WHERE `WifiID` = :value");
    
            //if no results then display default reviews 
            if(!$reviews->execute(array(':value' => $wifiID))){
                return NULL;
            }

            //$rows = $reviews->fetchAll(); 
            return $reviews;

		} catch (PDOException $ex) {
            return ERROR;
		}
    }


//Log in Page Querys & Inserts

    function insertUser($userid, $bday, $hashed, $email, $pdo){
       try{                
            $statement = $pdo->prepare(" INSERT INTO `members` (`UserID`, `Email`, `Birthdate`, `Password`) VALUES (:userid, :email, :bday, :hashed1);");
            if ($insert_result = $statement->execute(array(':userid' => $userid, ':email' => $email, ':bday' => $bday, ':hashed1' => $hashed))){
                $registered = true;
                $re_password = false;
                $re_email = false;
                echo  "<script>alert(\"Successfully Registered\");</script>";
                return true;
            } else {
                echo  "<script>alert(\"Error: while trying to register\");</script>";
                return ERROR;
            } 
        } catch (PDOException $ex) {
            echo  "<script>alert(\"Error: while trying to register: Database error\");</script>";
            return ERROR;
        }
    }


    function getUserName($email, $pdo){
		try{
            $result = $pdo->prepare("SELECT `Email` FROM `members` WHERE `Email` = :value");
            if(!$result->execute(array(':value' => $email))){
                return ERROR;
            } else {
                return $result;}
		} catch (PDOException $ex) {
            return ERROR;
		}
    }

    //returns the hashed DB password as a string
    function getDBPassword($POST, $pdo2){
		try{
            $email = htmlspecialchars(($POST['email']));
            $DBpassword = $pdo2->prepare("SELECT `Password` FROM `members` WHERE `Email` LIKE :name ");
            if(!$DBpassword->execute(array(':name' => $email))){
                return ERROR;
            } else {
                $row = $DBpassword->fetch();
                $DBpasswordresult2 = $row[0];
                return $DBpasswordresult2;}
		} catch (PDOException $ex) {
            return ERROR;
		}
    }

//Review Functions

function userExists($email, $pdo){
    try{
        $userexists = $pdo->prepare("SELECT * FROM `members` WHERE `Email` LIKE :value");
        if(!$userexists->execute(array(':value' => $email))){
            return false;
        } else {
            if (($userexists->rowCount())!=0){
                return true;
            }
        }
    } catch (PDOException $ex) {
        return false;
    }
}


//Submit review Functions

    function getWifiID($name, $pdo){
        try{
            $wifiID = $pdo->prepare("SELECT  `WifiID` FROM `items` WHERE `WifiName` = :value");
            if (!$wifiID->execute(array(':value' => $name))){
                return false;
            } else {
                if (($wifiID->rowCount())!=0){
                    $wifiID_string = $wifiID->fetchColumn(); 
                    return  $wifiID_string;
                }
            }
        } catch (PDOException $ex) {
            return false;
        }
    }

    function getRatings($wifiID_string, $pdo){
        try{
            $ratings = $pdo->prepare("SELECT  `Rating` FROM `reviews` WHERE `WifiID` LIKE :wifiID");
            if (!$ratings->execute(array(':wifiID' => $wifiID_string))){
                return false;
            } else {
                return  $ratings;
            }
        } catch (PDOException $ex) {
            return false;
        }
    }

    function getUserID($email, $pdo){
        try{
            $userID = $pdo->prepare("SELECT  `UserID` FROM `members` WHERE `Email` = :value");
            if (!$userID->execute(array(':value' => $email))){
                return false;
            } else {
                $userID_string = $userID->fetchColumn();

                return  $userID_string;
            }
        } catch (PDOException $ex) {
            return NULL;
        }
    }

    function insertReview($userID_string, $rating, $review, $wifiID_string, $pdo){
        try{
            $statement = $pdo->prepare(" INSERT INTO `reviews` (`UserID`, `ReviewText`, `Rating`, `WifiID`, `Date`) VALUES ( :userID, :review, :rating, :wifiID, '')");
            if (!$insert_result = $statement->execute(array(':userID' => $userID_string, ':review' => $review, ':rating' => $rating, ':wifiID'=> $wifiID_string))){
                return false;
            } else {
                return  true;
            }
        } catch (PDOException $ex) {
            echo  "<script>alert(\"DB exception error\");</script>";

            return false;
        }
    }

    function insertNewRating($new_rating, $wifiID_string, $pdo){
        try{
            $statement = $pdo->prepare("UPDATE `items` SET `Rating`= :rating WHERE `WifiID`= :wifiID");
            if (!$insert_result = $statement->execute(array(':rating' => $new_rating, ':wifiID' => $wifiID_string))){
                return false;
            } else {
                return  true;
            }
        } catch (PDOException $ex) {
            return NULL;
        }
    }




	/* End database functions */

?>