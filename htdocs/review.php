
<?php    
session_start();
//Header
	$page_title = "Wifi | Review";
    include 'include/header.php';
    include 'include/main.php';    

    //Globals 
    $userLoggedIn;

    //Check that the session user exists in the database... 
    if (isset($_SESSION['User'])){
        include 'include/database_connection.php';    
        include 'include/database_library.php'; 

        $email = htmlspecialchars($_SESSION['User']);

        if (userExists($email, $pdo)){
            $userLoggedIn = true;
        } else {
            header("Location: http://localhost/registration.php");
        }
    } else {
        $userLoggedIn = false;
    }
?>

<body>
	<div class="bgimg-1">
            <div class="layer">
            </div>
            <?php
                include 'include/menu.php'
            ?>
        <center>
            <br>
            <div class = "heading"> Leave A Review </div>
            <br>
            <br>	
            <br>
            
            <?php 
                //show review form to loged in user
                if ($userLoggedIn){
                    include 'include/review_form.php';
                }
            ?>

        </center>
    </div>
<body>
<?php
	include 'include/footer.php';
?>
<html>
