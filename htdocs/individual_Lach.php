<?php
session_start();
?>

<!DOCTYPE html>
<html>
    
<head>

    <style>
        
	
	
    body, html {
    height: 130%;
    margin: 0;
    font: 400 15px/1.8 "Lato", sans-serif;
    color: #fff;
    }


    .footer {
        font: 400 15px/1.8 "Lato", sans-serif;
        color: #fff;
        text-align: center;
    }

    
    .footerheading {
        color: #fff;
        text-align: center;

    }
    .footerinfo {
        font: 400 15px/1.8 "Lato", sans-serif;
        color: #fff;
        text-align: center;

    }

    .layerfooter {
    background-color: rgb(0, 18, 46);
    position: absolute;

    opacity: 0.9;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
	
    }

    .bgimg-2 {
    background-image: url("people.jpg");
    height: 20%;
	
    }

    .bgimg-1, .bgimg-2, .bgimg-3 {
    position: relative;
    opacity: 0.8;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
	
    }

    .bgimg-1 {
    background-image: url("people.jpg");
    height: 200%;
	
    }


	.layer {
    background-color: rgb(33, 48, 70);
    position: absolute;
    opacity: 0.7;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
    }

  
    .parent {
        display: flex;
        /* justify-content: flex-start; */
        justify-content: center;
        align-items: center;
    }
    .menu {
    opacity: 1; 
    text-align:center;
    position: relative;
    display: inline-block;
    border-bottom: 1px solid white;

    }

    .menu a {
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 35px;
    }

    .menu a:hover {
    color: black;
    }

    .menu a.active {
    color: white;
    }

        

    .heading {
        color: #ffffff;
        float: center;
        font-family: Oswald;
        font-size: 30px; 
        z-index: 1000;

    }
	
	.container {
    position: relative;
    text-align: center;
    color: white;
	font-family: Oswald;
    font-size: 30px;
	padding-top: 1%;
	width: 1600px;
	
	
}

	.heading {
        text-shadow: 3px 2px black;        
        position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
        font-family: Oswald;
        font-size: 30px; 

    }
	
		
	.grid-container2 {
	position: relative;
	display: grid; 
	width: 820px;
	margin-left: 325px;
	
	margin-top: -445px;
	grid-template-columns: auto auto ;
	background-color: #F0FFFF;
	padding: 5px 5px;
	
	
	
	}
	
	.grid-reviews {
	
	background-color: #F0FFFF;
	border: 1px solid  #fff;
	padding: 1px;
	font-size: 20px;
	color: #000000;
	text-align: center;
	height: 100px;
	
}

	.box {
	margin-left: 325px;
	width: 800px;
	height: 200px;
	display: inline-block;
	margin-bottom: 25px;
    position: relative;
    padding: 10px;
    border: 5px solid white;
	
    
}

	.checked {
		color: orange;
}

	#google-map {
		margin-top: 20px;
		margin-left: 325px;
	
		
	}
	
	.button {
		background-color: #f44336;
		color: white;
		padding: 15px 32px;
		text-align: center;
		text-decoration: none;
		font-size: 16px;
		cursor: pointer;
}

	.gallery {
		display: block;
		margin-top: -200;
		margin-right: 325px;
		margin-bottom: 20px;    
		border-color:  #ffffff; 
		width: 200px;
		text-align: center;
		margin-left: 58%;
}

	.heading2 {
        color: #ffffff;  
		margin-top: -270px;
        margin-left: 1175px;
		
		font-size: 30px; 

    }
	
	.bottom-right {
    position: absolute;
    bottom: 50px;
    right: 200px;
}

.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}

 /* Modal Header */
.modal-header {
    padding: 2px 16px;
    background-color: #d90429;;
    color: white;
}

/* Modal Body */
.modal-body {padding: 2px 16px;}

/* Modal Footer */
.modal-footer {
    padding: 2px 16px;
    background-color: #d90429;;
    color: white;
}

/* Modal Content */
.modal-content {
    position: relative;
    background-color: #F5FFFA;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 80%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    animation-name: animatetop;
    animation-duration: 0.4s
	
}

/* Add Animation */
@keyframes animatetop {
    from {top: -300px; opacity: 0}
    to {top: 0; opacity: 1}
} 

.header2 {
		color: #ffffff;
        font-family: Oswald;
        font-size: 20px; 
        z-index: 1000;
		display: inline;
	}
	
	.rating {
    unicode-bidi: bidi-override;
    direction: rtl;
    display: table-cell;
    position: relative;
    vertical-align: middle;
    padding: 0px 0px;
    color: rgb(83, 95, 112);
    border: none;
    white-space: nowrap;
    text-align: center;
    }
    .rating > span {
    display: inline-block;
    position: relative;
    width: 1.1em;
    }
    .rating > span:hover:before,
    .rating > span:hover ~ span:before {
    content: "\2605";
    position: absolute;
    }

	
		
	   
    </style>
</head>
<body>



    <div class="bgimg-1">
    
            <div class="layer">
            </div>
    
            <div class="parent">
                <div class="menu" >
                    <a class="active" href="https://cab230.sef.qut.edu.au/Students/n9441638/search_home.html">WiFind</a>
                    <a href="https://cab230.sef.qut.edu.au/Students/n9441638/results_chermside.html">Listings</a>
                    <a href="https://cab230.sef.qut.edu.au/Students/n9441638/individual_result v2.html">Page</a>
                    <a href="https://cab230.sef.qut.edu.au/Students/n9441638/registration_page.html">Register</a>
            </div>

                    
            </div>
			
    <center>
	<div class = "container3">
        <div class="container">
            <img src="delaware2.jpeg" alt="Delaware Street">
            <?php
            echo '<div class="heading">'.implode("",$_GET).'</div>';
             //echo '<div class="heading">'.$_GET['']</div>
             ?>
            
			</div>
<!--             the form needs to be reformatted with its own class
 -->            <button id="myBtn">Leave A Review</button>
 
 <!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    
  </div>
 

		
		
<div class="modal-content">
  <div class="modal-header">
    <span class="close">&times;</span>
    <h2>Leave a Review</h2>
  </div>
  <div class="modal-body">
     <?php //validate user php 
		//echo var_dump($_SESSION);
		if (isset($_SESSION['User'])){
        $mysqli3 = new mysqli("localhost", "root", "", "wifind");
        //echo "there is a session user";

        $email = $_SESSION['User'];

        $userexists = $mysqli3->query("SELECT * FROM `members` WHERE `Email` LIKE '$email' ");
        
        //echo var_dump($userexists);
        if (mysqli_num_rows($userexists)!=0){
		echo '<form action="http://localhost/submitreview.php" method="post">
             <br>
            <input style="height:200px;width:600px;font-size:14pt; type="text" name="Review">
            <br>
			<div class = "header2"> Rating </div>
			 <div class="rating">
                            <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                    </div>
			
            <br><br>
            <input type="submit" value="Submit">
          </form> ';
        }


    } else {
        echo "no session user ";

    }
		?>
  </div>
  <div class="modal-footer">
    
  </div>
</div>
</div> 
				
 
                


  

		
	</center>
		
		<div class = "box">
        <?php    

        $mysqli2 = new mysqli("localhost", "root", "", "wifind");

            $name = $_GET['name'];
            //secho var_dump($_GET);
            $result = $mysqli2->query("SELECT  `Address` FROM `items` WHERE `WifiName` = 'Annerley Library Wifi'");
            //echo var_dump($result);
            $row = $result->fetch_row();
            echo '<h3>'.$row[0].'</h3>';

        ?>
        <p> This wi-fi hotspot is on <?php echo $row[0] ?>, which is close to the C&K Marchant Park Kindergarten Assoc Inc 
			and offers easy access to the Seventh Brigade Park Cricket Fields. </p>
		</div>
		<div class = "heading2"> Photos from <?php echo $_GET['name'] ?> </div>
		
		
		
		<div class = "gallery">
		
		<img src="delaware3.jpeg" class="gallery" href="delaware3.jpeg" style = "width:300px; height:200px;" />
		<img src="delaware4.jpg" class="gallery" href="delaware4.jpg" style = "width:300px; height:200px;" />
		<img src="delaware5.jpeg" class="gallery" href="delaware5.jpeg"	style = "width:300px; height:200px;"  />
		</div>
		 
		<div class="grid-container2">
						
						<div class="grid-reviews"><img src="index.png" alt="profile photo" style="width:100px;height:100px;"></div>						
						<div class="grid-reviews"><i>"Had some really good times using this hotspot."</i></div>
						<div class="grid-reviews"><b>Great Wi-fi Hotspot</b></div>
						<div class="grid-reviews"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
						</div>
								
						<div class="grid-reviews"><img src="index.png" alt="profile photo" style="width:100px;height:100px;"></div>		
						<div class="grid-reviews"><i>"Just okay speeds for me."</i></div>
						<div class="grid-reviews"><b>Pretty okay Wi-fi Hotspot</b></div>
						<div class="grid-reviews"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span></div>
						
		</div>
		
		
		
		<div id="google-map">
					<iframe src="https://www.google.com/maps/d/embed?mid=1hDB9qdGsDx_Rwl_X7VXAbiWH09vRTovN" 
					width="1240" height ="450"></iframe> 
		</div>
		
      </div>  
		
	




        
      	   
		
		
		
	
	</div>
	
</body>	

<footer>
        <div class="bgimg-2">
            <div class="layerfooter">
            </div>

            <div >
                <h3 class="footerheading">CONTACT INFORMATION</h3>
                <div class="footerinfo">
                    <ul>
                        <li style="list-style-type:none">
                                <font color="white">Proudly Made in Australia</font>
                        </li>
                        <li style="list-style-type:none">
                            
                                <font color="white">Cell Phone</font>
                                <font color="white">+0447445861</font>
                        </li>
                        <li style="list-style-type:none">
                                <font color="white">E-mail Address</font>
                            <a href="mailto:georgefitzgerald2718@gmail.com" >georgefitzgerald2718@gmail.com</a>
                        </li>
                    </ul>
                </div>

            <div >
                <font color="white">Copyright GeorgeAndLachy © 2018. All Rights Reserved</font>
            </div>

			</div>
		</div>


</footer>

       







</html>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>